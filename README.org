#+TITLE: README
#+AUTHOR: Abraham Raji
#+EMAIL: abrahamraji99@gmail.com
#+STARTUP: overview
#+CREATOR: avronr
#+LANGUAGE: en

[[https://i.imgur.com/SfBcmPR.png]]

* My Dot files
** Dependencies
- KDE
- [[https://github.com/Airblader/i3][i3-gaps]]
  - General Instruction to installing i3-gaps
    - Arch / It's derivatives
    i3-gaps is also available from the official Arch Community Repository as i3-gaps.
    
    The development version of i3-gaps is available from the AUR as i3-gaps-next-git.
    
    For Manjaro there is also a dedicated i3 community spin which comes with a preconfigured system using i3-gaps.
    - NixOS
    Starting with NixOS 16.09, you can configure i3-gaps as default window manager in your /etc/nixos/configuration.nix:
    
    #+BEGIN_SRC shell
    services.xserver.windowManager = {
      i3-gaps.enable = true;
      default = "i3-gaps";
    };
    #+END_SRC
    - Void
    #+BEGIN_SRC 
    xbps-install -S i3-gaps
    #+END_SRC
    - Fedora
    i3-gaps is available as community repo on copr.
    - OpenSUSE
    i3-gaps is available from the official repository starting with Tumbleweed.
    - Gentoo
    #+BEGIN_SRC shell
     emerge i3-gaps
    #+END_SRC
  - Building from scratch 
   - libxcb-xrm-dev
     - Debian/Ubuntu
    There is a ppa maintained for libxcb-xrm-dev for Debian/Ubuntu
    #+BEGIN_SRC shell
    sudo add-apt-repository ppa:aguignard/ppa
    sudo apt-get update
    sudo apt-get install libxcb-xrm-dev
    #+END_SRC
     - From source
    for others or if the ppa fails stay upto date
    #+BEGIN_SRC shell
    mkdir tmp
    cd /tmp
    git clone https://github.com/Airblader/xcb-util-xrm
    cd xcb-util-xrm
    git submodule update --init
    ./autogen.sh --prefix=/usr
    make
    sudo make install
    #+END_SRC
   - i3-gaps
     #+BEGIN_SRC shell
     cd /tmp
     git clone https://www.github.com/Airblader/i3 i3-gaps
     cd i3-gaps
     git checkout gaps && git pull
     autoreconf --force --install
     rm -rf build
     mkdir build
     cd build
     ../configure --prefix=/usr --sysconfdir=/etc
     make
     sudo make install
     #+END_SRC
- [[https://py3status.readthedocs.io/en/latest/index.html][py3status]]  
- Feh
- Emacs
- Firefox
- cmus

*Feel free to fork and use these dotfiles and suggest improvements. :D*
